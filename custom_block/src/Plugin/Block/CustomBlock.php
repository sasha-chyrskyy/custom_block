<?php

namespace Drupal\custom_block\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with 5 last nodes.
 *
 * @Block(
 *   id = "custom_block",
 *   admin_label = @Translation("Custom block"),
 *   category = @Translation("Custom block"),
 * )
 */
class CustomBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Date Formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a new MyBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The Date Formatter.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    DateFormatter $dateFormatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the last 5 nodes.
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('status', 1)
      ->condition('created', strtotime('-2 hours'), '>=')
      ->sort('created', 'DESC')
      ->range(0, 5)
      ->execute();
    $results = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    $nodes = [];
    foreach ($results as $node) {
      $nid = $node->id();

      $nodes[$nid] = [
        'nid' => $nid,
        'title' => $node->getTitle(),
        'created' => $this->dateFormatter->format($node->getCreatedTime(), 'short'),
      ];

      if ($node->hasField('field_link')) {
        if (!$node->get('field_link')->isEmpty()) {
          $nodes[$nid]['link'] = $node->get('field_link')->first()->getValue();
        }
      }
    }

    return [
      '#theme' => 'custom_block',
      '#type' => 'block',
      '#nodes' => $nodes,
      '#attached' => [
        'library' => [
          'custom_block/custom-block-styles',
        ],
      ],
    ];
  }
}
